module github.com/zalleon/playground

go 1.12

require (
	github.com/prometheus/client_golang v0.9.3
	github.com/spf13/viper v1.5.0
)
