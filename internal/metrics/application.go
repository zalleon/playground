package metrics

import "github.com/prometheus/client_golang/prometheus"

type ApplicationMetrics struct {
	Version   string
	Commit    string
	BuildTime string

	RequestTotal *prometheus.CounterVec
	FailedRequestTotal *prometheus.CounterVec
}
