package config

import (
	"os"
	"testing"
)

func TestReadDefault_Success(t *testing.T) {
	cfg, err := Read()
	if err != nil {
		t.Errorf("Unable to read config: %s", err.Error())
		return
	}

	if cfg.Server.Connection.Host != "127.0.0.1" {
		t.Errorf("Invalid default server host: %s", cfg.Server.Connection.Host)
	}

	if cfg.Server.Connection.Port != 8080 {
		t.Errorf("Invalid default server port: %d", cfg.Server.Connection.Port)
	}
}

func TestReadFromENV_Success(t *testing.T) {

	_ = os.Setenv("PLAYGROUND_SERVER_CONNECTION_HOST", "0.0.0.0")
	_ = os.Setenv("PLAYGROUND_SERVER_CONNECTION_PORT", "80")

	cfg, err := Read()
	if err != nil {
		t.Errorf("Unable to read config: %s", err.Error())
		return
	}

	if cfg.Server.Connection.Host != "0.0.0.0" {
		t.Errorf("Invalid default server host: %s", cfg.Server.Connection.Host)
	}

	if cfg.Server.Connection.Port != 80 {
		t.Errorf("Invalid default server port: %d", cfg.Server.Connection.Port)
	}
}

