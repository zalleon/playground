package config

import (
	"strings"

	"github.com/spf13/viper"
)

type Config struct {
	Server    server
	Cache     cache
	Postgres  postgres
	Warehouse warehouseGRPC
}

type server struct {
	Connection hostPort
}

type cache struct {
	Connection hostPort
}

type postgres struct {
	Connection hostPort
}

type warehouseGRPC struct {
	Connection hostPort
}

type hostPort struct {
	Host string
	Port int
}

func Read() (Config, error) {
	viper.SetEnvPrefix("PLAYGROUND")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	viper.SetDefault("server.connection.host", "127.0.0.1")
	viper.SetDefault("server.connection.port", "8080")

	cfg := Config{}
	err := viper.Unmarshal(&cfg)

	return cfg, err
}
