
GITREV?=$(shell git log -1 --pretty=format:"%H" || echo undefined)
DATE?=$(shell date -u "+%Y-%m-%d %H:%M:%S")
LDFLAGS="-s -extldflags '-static' \
	-X 'main.AppVersion=${IMAGE_TAG}' \
	-X 'main.GitRev=${GITREV}' \
	-X 'main.BuildDate=${DATE}'"

.PHONY: deps
deps:
	GO111MODULE=${GO111MODULE} go get ./... && \
	GO111MODULE=${GO111MODULE} go mod tidy
#	GO111MODULE=${GO111MODULE} go mod vendor


.PHONY: lint
lint:
ifeq (, $(shell which golangci-lint))
	$(error "No golangci-lint in $(PATH). Install it from https://github.com/golangci/golangci-lint")
endif
	golangci-lint run


.PHONY: test
test:
	GO111MODULE=${GO111MODULE} go test -mod vendor ./...


.PHONY: build
build: clean
ifndef IMAGE_TAG
	IMAGE_TAG=v0.1.0-dev $(MAKE) build
else
	@echo "+ $@"
	GO111MODULE=${GO111MODULE} go build \
		-mod vendor \
		-tags "netgo std static_all" \
		-ldflags $(LDFLAGS) \
		-o ${OUTPUT} cmd/server/main.go
endif
